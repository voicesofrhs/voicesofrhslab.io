Voices Of RHS Ideas
===================
Articles
--------
P	club of the week	Drama club	DeAndre
 	news	Librarian Retiring	Elsa
P	news	Newspaper VS Trivory	Ellie
p	news	U of P jazz festival	Victoria
p	editorial	Single Stall Bathrooms	Kennedy
p	editorial	Mask Mandate Drop	Jerry

PA	news	Unity Fest		Vivian
 A	news/arts	Poetry slam	
PA	news	CS Competition		Johnny

Columns
-------
- News
- Editorials
- Club of the week
- Arts - Cristina
- Poetry column - elanor
- Q&A - keenan
- Sports - calder

What I Need
-----------
- A Banner (+ square variant)
- Search and hamburger icons
- Redesigned Voices logo
- Cropped images
- Articles
- Club Of The Week photo and description

Personal Todo
-------------
- Website main page
- Website about page
- Website category pages
- Website community art page
